import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "summary",
      component: require("@/views/Summary.vue")
    },
    {
      path: "/:path",
      name: "path",
      component: require("@/views/Path.vue"),
      props: true
    }
  ]
});
